package com.bg.heavencoffeeapi.controller;

import com.bg.heavencoffeeapi.entity.People;
import com.bg.heavencoffeeapi.model.unlucky.UnluckyChangeRequest;
import com.bg.heavencoffeeapi.model.unlucky.UnluckyItem;
import com.bg.heavencoffeeapi.model.unlucky.UnluckyRequest;
import com.bg.heavencoffeeapi.model.unlucky.UnluckyResponse;
import com.bg.heavencoffeeapi.service.PeopleService;
import com.bg.heavencoffeeapi.service.UnluckyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/unlucky")
public class UnluckyController {
    private final PeopleService peopleService;
    private final UnluckyService unluckyService;

    @PostMapping("/new/people-id/{peopleId}")
    public String setUnlucky(@PathVariable long peopleId, @RequestBody UnluckyRequest request) {
        People people = peopleService.getData(peopleId);
        unluckyService.setUnlucky(people, request);

        return "OK";
    }

    @GetMapping("/all")
    public List<UnluckyItem> getUnluckys() {
        return unluckyService.getUnluckys();
    }
    @GetMapping("/detail/{id}")
    public UnluckyResponse getUnlucky(@PathVariable long id) {
        return unluckyService.getUnlucky(id);
    }

    @PutMapping("/unlucky/unlucky-id/{unluckyId}")
    public String putUnlucky(@PathVariable long unluckyId, UnluckyChangeRequest request) {
        unluckyService.putUnlucky(unluckyId, request);

        return "OK";
    }
    @DeleteMapping("/{id}")
    public String delUnlucky(@PathVariable long id) {
        unluckyService.delLucky(id);

        return "OK";
    }
}
