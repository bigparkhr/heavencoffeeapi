package com.bg.heavencoffeeapi.controller;

import com.bg.heavencoffeeapi.model.people.PeopleRequest;
import com.bg.heavencoffeeapi.service.PeopleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/people")
public class PeopleController {
    private final PeopleService peopleService;

    @PostMapping("/join")
    private String setPeople(PeopleRequest request) {
        peopleService.setPeople(request);

        return "Ok";
    }
}
