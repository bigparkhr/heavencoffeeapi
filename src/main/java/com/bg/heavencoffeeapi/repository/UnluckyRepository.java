package com.bg.heavencoffeeapi.repository;

import com.bg.heavencoffeeapi.entity.Unlucky;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnluckyRepository extends JpaRepository<Unlucky, Long> {
}
