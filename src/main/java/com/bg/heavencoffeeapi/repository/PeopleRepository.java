package com.bg.heavencoffeeapi.repository;

import com.bg.heavencoffeeapi.entity.People;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PeopleRepository extends JpaRepository<People, Long> {
}
