package com.bg.heavencoffeeapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Unlucky {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "peopleId", nullable = false)
    private People people;

    @Column(nullable = false)
    private LocalDate dateShit;

    @Column(nullable = false)
    private Double priceHoly;
}
