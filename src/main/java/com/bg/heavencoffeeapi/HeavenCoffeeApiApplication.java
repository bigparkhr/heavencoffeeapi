package com.bg.heavencoffeeapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HeavenCoffeeApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(HeavenCoffeeApiApplication.class, args);
    }

}
