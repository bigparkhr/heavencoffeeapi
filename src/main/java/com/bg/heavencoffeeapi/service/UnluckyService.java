package com.bg.heavencoffeeapi.service;

import com.bg.heavencoffeeapi.entity.People;
import com.bg.heavencoffeeapi.entity.Unlucky;
import com.bg.heavencoffeeapi.model.unlucky.UnluckyChangeRequest;
import com.bg.heavencoffeeapi.model.unlucky.UnluckyItem;
import com.bg.heavencoffeeapi.model.unlucky.UnluckyRequest;
import com.bg.heavencoffeeapi.model.unlucky.UnluckyResponse;
import com.bg.heavencoffeeapi.repository.UnluckyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UnluckyService {
    private final UnluckyRepository unluckyRepository;

    public void setUnlucky(People people, UnluckyRequest request) {
        Unlucky addData = new Unlucky();
        addData.setPeople(people);
        addData.setDateShit(request.getDateShit());
        addData.setPriceHoly(request.getPriceHoly());

        unluckyRepository.save(addData);
    }

    public List<UnluckyItem> getUnluckys() {
        List<Unlucky> originList = unluckyRepository.findAll();

        List<UnluckyItem> result = new LinkedList<>();

        for (Unlucky unlucky: originList) {
            UnluckyItem addItem = new UnluckyItem();
            addItem.setId(unlucky.getId());
            addItem.setName(unlucky.getPeople().getName());
            addItem.setDateShit(unlucky.getDateShit());
            addItem.setPriceHoly(unlucky.getPriceHoly());

            result.add(addItem);
        }
        return result;
    }
    public UnluckyResponse getUnlucky(long id) {
        Unlucky originData = unluckyRepository.findById(id).orElseThrow();

        UnluckyResponse response = new UnluckyResponse();
        response.setId(response.getId());
        response.setPeople(response.getPeople());
        response.setName(response.getName());
        response.setDateShit(response.getDateShit());
        response.setPriceHoly(response.getPriceHoly());

        return response;
    }

    public void putUnlucky(long id, UnluckyChangeRequest request) {
        Unlucky originData = unluckyRepository.findById(id).orElseThrow();
        originData.setPriceHoly(request.getPriceHoly());
        originData.setDateShit(request.getDateShit());

        unluckyRepository.save(originData);
    }
    public void delLucky(long id) {
        unluckyRepository.deleteById(id);
    }
}
