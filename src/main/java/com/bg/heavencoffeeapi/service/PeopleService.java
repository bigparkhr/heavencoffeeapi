package com.bg.heavencoffeeapi.service;

import com.bg.heavencoffeeapi.entity.People;
import com.bg.heavencoffeeapi.model.people.PeopleRequest;
import com.bg.heavencoffeeapi.repository.PeopleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PeopleService {
    private final PeopleRepository peopleRepository;

    public People getData(long id) {
        return peopleRepository.findById(id).orElseThrow();
    }

    public void setPeople(PeopleRequest request) {
        People addData = new People();
        addData.setName(request.getName());

        peopleRepository.save(addData);
    }
}
