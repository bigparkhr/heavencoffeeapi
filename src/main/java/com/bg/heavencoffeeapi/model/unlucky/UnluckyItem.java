package com.bg.heavencoffeeapi.model.unlucky;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class UnluckyItem {
    private Long id;
    private String name;
    private LocalDate dateShit;
    private Double priceHoly;
}
