package com.bg.heavencoffeeapi.model.unlucky;

import com.bg.heavencoffeeapi.entity.People;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class UnluckyResponse {
    private Long id;
    private String name;
    private People people;
    private LocalDate dateShit;
    private Double priceHoly;
}
