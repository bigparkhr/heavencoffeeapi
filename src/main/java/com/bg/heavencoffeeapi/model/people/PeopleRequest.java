package com.bg.heavencoffeeapi.model.people;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PeopleRequest {
    private String name;
}
